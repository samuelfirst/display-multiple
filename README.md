# display-multiple

Wrapper around the ImageMagick 'display' command to allow images to be shown in
a slideshow format.

The purpose of this script is to fix issues with displaying multiple images of
differing sizes (with some larger than the display).  When using the built-in
options for 'display', images that are larger than the screen resolution are
not automatically resized to fit entirely on the screen.  The resize flag also
does not provide an easy way to resize it to fit in the screen resolution (at
least as far as I can tell).  This script fixes this issue by comparing the
size of the image to the screen resolution and calculating the minimum amount
that it needs to be resized to fit fully on the screen, then passing that to
'resize'.

## Usage

No fancy argument parsing at the moment, just call the script with the names
of the files you want to display.  There's a slight delay (0.2s) between each
file being displayed where you can kill the script with C-c to exit prematurely.

### Examples

* Display files: `display-multiple file_1 file_2 file_3`
* Display all the files in a directory: `display-multiple ./*`
* Display all the files in a directory in a random order: `display-multiple.awk $(awk 'BEGIN{for (i = 1; i < ARGC; i++) printf ARGV[i] "\n"}' ./* | shuf -)`

## Dependencies
* awk
* xrandr (and, by extension X)
* ImageMagick suite
** display
** identify